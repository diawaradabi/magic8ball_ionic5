import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';

import { HomePage } from './home.page';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage,],
      imports: [IonicModule.forRoot(), RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("retourne un tableau non vide", ()=>{
  let tabresultat=component.getAnswers();
  expect(Array.isArray(tabresultat)).toBeTruthy;
  expect(tabresultat.length).toBeGreaterThan(0);
});

it("doit returné un random de type string", ()=>
  {
    expect(typeof component.giveAnswer()).toBe("string");
  });
})