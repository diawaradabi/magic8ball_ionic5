import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public cssClass: string;
  private answers = ["C'est certain.", 'Il en est décidément ainsi.', "Sans aucun doute.",
  'Oui - certainement.', 'Vous pouvez vous y fier.', 'Comme je le vois, oui.',
  "Très probablement.", "Perspectives bonnes.", "Oui.",
  'Les signes pointent vers oui.', 'Répondre brumeux, réessayer.', 'Demander plus tard.',
  'Mieux vaut ne pas vous le dire maintenant.', 'Impossible de prédire maintenant.',
  "Concentrez-vous et demandez à nouveau.", "Ne comptez pas dessus.", "Ma réponse est non.",
  "Mes sources disent non.", "Les perspectives ne sont pas si bonnes.", "Très douteux."]
   answer:string;

  constructor(private toastCtrl: ToastController) {}


  getAnswers()
  {
      return this.answers;
  }  
  giveAnswer() {
    this.cssClass = "animated shake";
     this.answer= this.answers[Math.floor(Math.random() * this.answers.length)];
    // this.presentToast(answer);

    var temp = this;
    setTimeout(function(){
      temp.presentToast(this.answer);
    }, 2000)
    return this.answer;
  }

  async presentToast(answer) {
    const toast = await this.toastCtrl.create({
      message: this.answer,
      duration: 500,
      position: 'top'
    });

    toast.onDidDismiss().then(() => {
      console.log('toast dismissed');
      this.cssClass = "";
    });
    toast.present();
  }

}
